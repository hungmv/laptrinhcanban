// Mã Văn Hùng - LTCB_L03_AT19E
#include <stdio.h>

// Tìm số lớn của 2 số được nhập từ bàn phím

void solon()
{
    int a, b;
    printf("Nhap so thu nhat: ");
    scanf("%d", &a);
    printf("Nhap so thu hai: ");
    scanf("%d", &b);
    if (a > b)
    {
        printf("So lon nhat la: %d", a);
    }
    else
    {
        printf("So lon nhat la: %d", b);
    }
}

// Tính giai thừa của số nguyên n

// đệ quy

double tinhgiaithua(int n)
{
    if (n > 0)
        return n * tinhgiaithua(n - 1);
    else
        return 1;
}
// Tính giai thừa không đệ quy
void giaithua()
{
    int n;
    printf("Nhap n: ");
    scanf("%d", &n);
    int gt = 1;
    for (int i = 1; i <= n; i++)
    {
        gt *= i;
    }
    printf("Giai thua cua %d la: %d", n, gt);
}

// Tính a mũ m
void somu()
{
    int a, b;
    printf("Nhap a: ");
    scanf("%d", &a);
    printf("Nhap b: ");
    scanf("%d", &b);
    int kq = 1;
    for (int i = 1; i <= b; i++)
    {
        kq *= a;
    }
    printf("\nSo mu cua %d va %d la: %d", a, b, kq);
}

// Kiểm tra số nguyên tố

void snt()
{
    int n;
    printf("\nNhap n: ");
    scanf("%d", &n);
    int dem = 0;
    int kq[10];
    if (n < 2)
        printf("\nKhong co so nguyen to");
    else
    {
        for (int i = 2; i <= n; i++)
        {
            if (n % i == 0)
            {
                dem++;
            }
        }
        if (dem == 0)
            printf("\n%d la so nguyen to", n);
        else
            printf("\n%d khong phai so nguyen to", n);
    }
}

int main()
{
    int chon;

    do
    {
        printf("\n1. Tim so lon trong 2 so.");
        printf("\n2. Tinh giai thua cua n.");
        printf("\n3. Tinh a mũ b.");
        printf("\n4. Kiem tra so nguyen to.");
        printf("\n5. Tinh giai thua bang de quy.");
        printf("\n6. Thoat.");
        printf("\nNhap lua chon: ");
        scanf("%d", &chon);

        switch (chon)
        {
        case 1:
            solon();
            break;
        case 2:
            giaithua();
            break;
        case 3:
            somu();
            break;
        case 4:
            snt();
            break;
        case 5:
            int n;
            printf("\nNhap n: ");
            scanf("%d", &n);
            tinhgiaithua(n);
            break;
        }
    } while (chon < 5 || chon != (int)chon);
    return 0;

    return 0;
}
