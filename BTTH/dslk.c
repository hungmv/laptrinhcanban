//Mã Văn Hùng - LTCB_L03_AT19E

#include <stdio.h>

struct student {
    int id;
    char *name;
    struct student *next;
};
struct student *phead = NULL;

void add_node(struct student **pphead, int id, char *name) {
    struct student *new_node = (struct student *)malloc(sizeof(struct student));
    new_node->id = id;
    new_node->name = name;
    new_node->next = NULL;

    if (*pphead == NULL) {
        *pphead = new_node;
        return;
    }

    struct student *p = *pphead;
    while (p->next != NULL) {
        p = p->next;
    }
    p->next = new_node;
}
void add_data_node(struct student **pphead, int id, char *name) {
    printf("Enter number of nodes: ");
    int n;
    scanf("%d", &n);

    for (int i = 0; i < n; i++) {
        printf("Enter id: ");
        scanf("%d", &id);
        printf("Enter name: ");
        scanf("%s", name);
        add_node(&phead, id, name);
    }
}
void sort_by_id(struct student **pphead) {
    if (*pphead == NULL || (*pphead)->next == NULL) {
        return;
    }

    struct student *p1, *p2, *p3, *tmp;

    while (1) {
        p1 = NULL;
        p2 = *pphead;

        if (p2->next == NULL) {
            return;
        }

        p3 = p2->next;

        while (p3 != NULL) {
            if (p2->id > p3->id) {
                if (p1 != NULL) {
                    p1->next = p3;
                }
                tmp = p3->next;
                p3->next = p2;
                p2->next = tmp;

                if (p1 == NULL) {
                    *pphead = p3;
                }
                p1 = p3;
                p3 = tmp;
            } else {
                p1 = p2;
                p2 = p3;
                p3 = p3->next;
            }
        }
    }
}
void delete_node(struct student **pphead, int id) {
    struct student *p1, *p2;

    if (*pphead == NULL) {
        return;
    }

    if ((*pphead)->id == id) {
        struct student *tmp = (*pphead)->next;
        free(*pphead);
        *pphead = tmp;
        return;
    }

    for (p1 = *pphead; p1 != NULL; p1 = p1->next) {
        if (p1->id == id) {
            break;
        }
        p2 = p1;
    }

    if (p1 == NULL) {
        return;
    }

    struct student *tmp = p1->next;

    free(p1);
    p2->next = tmp;

}
void print_list(struct student *phead) {
    struct student *p = phead;
    while (p != NULL) {
        printf("%d %s\n", p->id, p->name);
        p = p->next;
    }
}

int main() {
    system("clear");
    int id;
    char name[100];

    

    while (1) {
        printf("1. Add node\n");
        printf("2. Sort by id\n");
        printf("3. Delete node\n");
        printf("4. Print list\n");
        printf("5. Exit\n");

        int choice;
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                add_data_node(&phead, id, name);
                break;
            case 2:
                sort_by_id(&phead);
                break;
            case 3:
                printf("Enter id: ");
                scanf("%d", &id);
                delete_node(&phead, id);
                break;
            case 4:
                print_list(phead);
                break;
            case 5:
                return 0;
            default:
                printf("Invalid choice\n");
        }
    }
}