//Mã Văn Hùng - LTCB_L03_AT19E

#include <stdio.h>

// D1C2: viết chương trình đếm và in ra các số nguyên tố có trong một mảng nguyên kích thước n được nhập từ bàn phím.

int main()
{
    int n, a[20];
    printf("Nhap n: ");
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        printf("Nhap a[%d]: ", i);
        scanf("%d", &a[i]);
    }
    int b[15], j = 0;
    for (int i = 0; i < n; i++)
    {
        int dem = 0;
        if (a[i] < 2)
            break;
        else
            for (int k = 2; k < a[i]; k++)
            {
                if (a[i] % k == 0)
                {
                    dem++;
                }
            }
        if (dem == 0)
        {
            b[j] = a[i];
            j++;
        }
    }
    printf("So nguyen to co trong mang la: %d\n", j);
    for (int i = 0; i < j; i++)
    {
        printf("%d", b[i]);
    }
    return 0;
}