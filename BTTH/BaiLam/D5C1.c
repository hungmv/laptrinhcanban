// Mã Văn Hùng - LTCB_L03_AT19E

//D5C1: Viết chương trình tính tổng s:=1^2 + 2^2 + 3^2 + …… + n^2 với n là số tự nhiên được nhập từ bàn phím (nếu n<=0 thì chương trình yêu cầu nhập lại).

#include <stdio.h>
#include <math.h>

int main()
{
    int n, s = 0;
    printf("Nhap n: ");
    scanf("%d", &n);
    if (n <= 0)
    {
        printf("Nhap lai n: ");
        scanf("%d", &n);
    }
    for (int i = 1; i <= n; i++)
    {
        s += pow(i, 2);
    }
    printf("Tong la: %d", s);
    return 0;
}