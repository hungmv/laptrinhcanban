#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct
{
    char masv[7];
    char hoten[30];
} SV;
typedef struct Node
{
    SV data;
    struct Node *next;

} node;
typedef struct
{
    node *head, *tail;
} list;
//khoi tao danh sach rong
void init(list *l)
{
    l->head=l->tail=NULL;
}
void nhap1sv(SV *sv)
{
    printf("\nNhap masv:\t");
    fflush(stdin);
    gets(sv->masv);
    printf("\nNhap hoten:\t");
    fflush(stdin);
    gets(sv->hoten);
}
node* taonode(SV *sv)
{
    node *p;
    p=(node*)malloc(sizeof(node));
    if(p==NULL) return NULL;
    p->data=*sv;
    p->next=NULL;
    return p;
}
void themdau(list *l, node *p)
{
    if (l->head==NULL)
    {
        l->head=l->tail=p;
        p->next=NULL;
    }
    else
    {
        p->next=l->head;
        l->head=p;
    }
}

void AddAfter(list *l, node *p, node *q)
{
    for(node *k=l->head; k!=NULL; k=k->next)
        if (k==q)
        {
            node *g=k->next;
            k->next=p;
            p->next=g;
        }
}

void removehead(list *l)
{
    node *p=l->head ;// p trỏ vào đầu
    l->head=l->head->next;// đầu trỏ sang bên cạnh
    free(p);// xóa p
}
void xoacuoi(list *l)
{
    node *p; //p là node nằm trước tail tìm thấy
    for(node *k=l->head; k!=NULL; k=k->next)
    {
        if (k==l->tail)
        {
            l->tail=p;//l.tail trỏ vào nơi p trỏ
            l->tail->next=NULL;
            free(k);//trong C++ dùng Delete k;
            return;
        }
        p=k;

    }
}
void xoasaumotnode(list *l, node *q)
{
    node *p;//node nằm sau q
    for(node *k=l->head; k!=NULL; k=k->next)
    {
        //tìm node  q
        if (k==q) //q chính là node k
        {
            p=k->next;
            k->next=p->next;
            free(p);
            return;
        }
    }
}


void xoasv(list *l)
{
    char ht[20];
    printf("\nNhap hoten can xoa: ");
    fflush(stdin);
    gets(ht);
    node *k;
    k=l->head;
    while (k!=NULL &&strcmp(k->data.hoten,ht)!=0)
    {
        k=k->next;
    }
    node *p;
    p=k;
    if (k==NULL) printf("\n khong tim thay phan tu can xoa");
    else
    {
        if (p==l->head) removehead(l);
        else if (p==l->tail) xoacuoi(l);
        else xoasaumotnode(l,p);
    }

}
void Nhapdssv(list *l, SV *sv)
{
    int n;
    printf("\nNhap so sinh vien: ");
    scanf("%d",&n);
    init(l);
    node *p;
    int i;
    for(i=0; i<n; i++)
    {
        printf("\nNhap sinh vien thu %d: ",i+1);
        nhap1sv(sv);
        p=taonode(sv);
        themdau(l,p);
    }
}
void xuat1sv(SV sv)
{
    printf("\n%7s|%20s|",sv.masv,sv.hoten);
}

void hoandoi(SV *a, SV *b)
{
    SV tg;
    tg=*a;
    *a=*b;
    *b=tg;
}
void xuatdssv(list l)
{
    node *k;
    printf("\n%7s|%20s|%8s|%4s|","masv","hoten","gioitinh","dtb");
    for(k=l.head; k!=NULL; k=k->next)
        xuat1sv(k->data);
}
void sapxeptang(list l)
{
    node *i, *j;
    for(i=l.head; i!=NULL; i=i->next)
        for (j=i->next; j!=NULL; j=j->next)
            if (i->data.masv>j->data.masv)
                hoandoi(&i->data,&j->data);
    xuatdssv(l);
}

int main()
{
    system("clear");
    list l;
    SV sv;

    while (1) {
        printf("\n1. Nhap sv");
        printf("\n2. Sap xep");
        printf("\n3. Xoa");
        printf("\n4. In DS ");
        printf("\n5. Thoat");

        int choice;
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                Nhapdssv(&l,&sv);
                break;
            case 2:
                sapxeptang(l);
                break;
            case 3:
                xoasv(&l);
                break;
            case 4:
                xuatdssv(l);
                break;
            case 5:
                return 0;
            default:
                printf("Nhap sai\n");
        }
    }
    return 0;
}
