// Mã Văn Hùng - LTCB_L03_AT19E

#include <stdio.h>

// D2C2: Nhập một dãy số thực a, kích thước n. Nhập một số thực x bất kỳ từ bàn phím. Kiểm tra xem số thực x  có trong dãy hay không,
// nếu có thì xuất hiện bao nhiêu lần và in ra các vị trí của nó có trong dãy.

int main()
{
    float a[100], x, n;
    printf("Nhap n: ");
    scanf("%f", &n);
    for (int i = 0; i < n; i++)
    {
        printf("Nhap a[%d]: ", i);
        scanf("%f", &a[i]);
    }
    printf("Nhap x: ");
    scanf("%f", &x);
    int dem = 0;
    int b[100], j = 0;
    for (int i = 0; i < n; i++)
    {
        if (a[i] == x)
        {
            dem++;
            b[j] = i;
            j++;
        }
    }
    if (dem != 0)
    {
        printf("\nSo %.2f Xuat hien tai vi tri: ", x);
        for (int i = 0; i < dem; i++)
        {
            printf("%d\t", b[i]);
        }
    }

    else
        printf("\nKhong co %.2f trong mang", x);
    return 0;
}