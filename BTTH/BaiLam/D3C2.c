// Mã Văn Hùng - LTCB_L03_AT19E
// D3C2: Nhập dãy số nguyên dương. Xét xem trong dãy có số hoàn hảo hay không (là số có tổng các ước của nó bằng chính nó:VD 6=1+2+3)?
// Nếu có hãy in ra các số hoàn hảo. Từ đó tìm số hoàn hảo nhỏ nhất trong dãy.

#include <stdio.h>

int main()
{
    int a[100], b[100], k = 0, n;
    printf("Nhap so luong phan tu: ");
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        printf("a[%d] = ", i);
        scanf("%d", &a[i]);
    }

    for (int i = 0; i < n; i++)
    {
        int sum = 0;
        for (int j = 1; j < a[i]; j++)
        {

            if (a[i] % j == 0)
            {
                sum += j;
            }
        }
        if (sum == a[i])
        {
            b[k] = a[i];
            k++;
        }
    }
    printf("Cac so hoan hao: ");
    for (int i = 0; i < k; i++)
    {
        printf("%d\t", b[i]);
    }
    int min = b[0];
    for (int i = 0; i < k; i++)
    {
        if (b[i] < min)
        {
            min = b[i];
        }
    }
    printf("\nSo hoan hao nho nhat: %d\n", min);
}
