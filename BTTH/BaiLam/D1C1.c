//Mã Văn Hùng - LTCB_L03_AT19E

#include <stdio.h>

//Giải và biện luận phương trình bậc hai ax2+bx+c = 0 với a ≠ 0, b, c được nhập bất kỳ từ bàn phím.

int main()
{
    float a, b, c, delta, x1, x2;
    printf("Nhap a b c: ");
    scanf("%f %f %f", &a, &b, &c);
    delta = b * b - 4 * a * c;
    if (delta < 0)
    {
        printf("Phuong trinh vo nghiem");
    }
    else if (delta == 0)
    {
        x1 = -b / (2 * a);
        printf("Phuong trinh co nghiem kep x = %f", x1);
    }
    else
    {
        x1 = (-b + sqrt(delta)) / (2 * a);
        x2 = (-b - sqrt(delta)) / (2 * a);
        printf("Phuong trinh co 2 nghiem phan biet x1 = %f va x2 = %f", x1, x2);
    }
    return 0;
}