// Mã Văn Hùng - LTCB_L03_AT19E

//D5C2: Viết chương trình nhập một xâu kí tự, cho biết:
    // a) Có bao nhiêu kí tự dạng chữ, dạng số, trắng và các kí tự đặc biệt.
    // b) Kí tự dạng nào là nhiều nhất?

int main()
{
    char ch;
    int chu = 0, so = 0, trang = 0, db = 0;
    printf("Nhap ky tu: ");
    scanf("%c", &ch);
    while (ch != '\n')
    {
        if (ch >= 'A' && ch <= 'Z')
        {
            chu++;
        }
        else if (ch >= 'a' && ch <= 'z')
        {
            so++;
        }
        else if (ch >= '0' && ch <= '9')
        {
            trang++;
        }
        else
        {
            db++;
        }
    }
    printf("\nSo ky tu dang chu: %d", chu);
    printf("\nSo ky tu dang so: %d", so);
    printf("\nSo ky tu dang trang: %d", trang);
    printf("\nSo ky tu dang db: %d", db);

    return 0;
}