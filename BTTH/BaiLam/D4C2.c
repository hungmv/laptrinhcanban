// Mã Văn Hùng - LTCB_L03_AT19E

//D4C2: Nhập dãy số thực a (sử dụng con trỏ). Sắp xếp dãy theo chiều giảm dần. 
// In ra dãy sau khi sắp xếp.
#include <stdio.h>
#include <stdlib.h>

int main()
{
    float *a;
    int n;
    int i, j;
    printf("Nhap so phan tu: ");
    scanf("%d", &n);
    a = (float *)malloc(n * sizeof(float));
    for (i = 0; i < n; i++)
    {
        printf("a[%d] = ", i);
        scanf("%f", &a[i]);
    }
    for (i = 0; i < n - 1; i++)
    {
        for (j = i + 1; j < n; j++)
        {
            if (a[i] > a[j])
            {
                float temp = a[i];
                a[i] = a[j];
                a[j] = temp;
            }
        }
    }
    for (i = 0; i < n; i++)
    {
        printf("%.2f ", a[i]);
    }
    return 0;


}