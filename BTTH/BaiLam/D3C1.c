// Mã Văn Hùng - LTCB_L03_AT19E
// D3C1: Tìm các số có 3 chữ số sao cho tổng lập phương của các chữ số bằng chính nó. (Ví dụ 1^3+5^3+3^3=153)

#include <stdio.h>


int main()
{
    int a, b, c;
    for (a = 1; a < 10; a++)
    {
        for (b = 0; b < 10; b++)
        {
            for (c = 0; c < 10; c++)
            {
                if (a * a * a + b * b * b + c * c * c == 100 * a + 10 * b + c)
                {
                    printf("%d\n", 100 * a + 10 * b + c);
                }
            }
        }
    }
    return 0;
}