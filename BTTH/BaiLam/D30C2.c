//Mã Văn Hùng - LTCB_L03_AT19E

#include <stdio.h>


// Xây dựng cấu trúc  học sinh gồm các thông tin: họ tên, điểm trung bình(DTB).
//     a. Viết chương trình nhập vào một danh sách học sinh. 
//     b. In ra danh sách các học sinh đạt kết quả khá (DTB >=7).
//     c. Tìm và in ra học sinh có điểm trung bình cao nhất.
//     1. Xây dựng cấu trúc  học sinh gồm các thông tin: họ tên, điểm trung bình (ĐTB).
//         a. Viết chương trình nhập vào một danh sách học sinh. 
//         b. In ra danh sách các học sinh đạt kết quả khá (điểm trung bình >=7).
//         c. Tìm và in ra học sinh có điểm trung bình cao nhất.
//         d. In ra danh sách học sinh có kết quả kém (ĐTB<5).
//         e. In ra danh sách học sinh có kết quả giỏi (ĐTB>=8).
//         f. Sắp xếp danh sách theo họ tên, in lại danh sách sau khi sắp xếp.
//     2. Nhập một dãy số nguyên từ file input.dat sắp xếp theo thứ tự tăng dần rồi ghi vào file output.dat
//     3. Viết hàm nhập 10 số thực từ bàn phím vào file văn bản tên là INPUT.DAT. 
//        Viết hàm đọc các số nguyên từ file trên và ghi những số chia hết cho 3 vào file OUTPUT1.DAT, những số chia 3 dư 1 vào file OUTPUT2.DAT, những số chia 3 dư 2 vào file OUTPUT3.DAT.
//     4. Cho một file n*m số dạng văn bản INPUT.DAT được ghi liên tục. Hãy đọc và ghi lại vào file RESULT.DAT thành dạng ma trận n dòng, m cột.



