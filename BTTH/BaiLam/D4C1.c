// Mã Văn Hùng - LTCB_L03_AT19E

//D4C1: Nhập vào một kí tự. Kiểm tra xem kí tự đó là chữ cái, chữ số hay các kí tự khác.

#include <stdio.h>

int main()
{
    char c;
    printf("Nhap ky tu: ");
    scanf("%c", &c);
    if (c >= 'A' && c <= 'Z')
    {
        printf("Ky tu %c la chu cai viet Hoa\n", c);
    }
    else if (c >= 'a' && c <= 'z')
    {
        printf("Ky tu %c la chu cai viet thuong\n", c);
    }
    else if (c >= '0' && c <= '9')
    {
        printf("Ky tu %c chu so\n", c);
    }
    else
    {
        printf("Ky tu %c la ky tu dac biet\n", c);
    }
    return 0;
}