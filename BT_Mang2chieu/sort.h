//Mã Văn Hùng - LTCB_L03_AT19E
#include <stdio.h>

void sort_arr()
{
    int n;
    printf("Nhap n: ");
    scanf("%d", &n);
    int a[n][n];
    int i, j, k;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("Nhap a[%d][%d]: ", i, j);
            scanf("%d", &a[i][j]);
        }
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("%2d ", a[i][j]);
        }
        printf("\n");
    }
    // Sắp xếp hàng thứ hai theo thứ tự giảm dần
    for (i = 0; i < n; i++)
    {
        for (k = i + 1; k < n; k++)
        {
            if (a[1][i] < a[1][k])
            {
                int temp = a[1][i];
                a[1][i] = a[1][k];
                a[1][k] = temp;
            }
        }
    }
    printf("Mang sau khi sap xep hang thu hai theo thu tu giam dan:\n");
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("%1d ", a[i][j]);
        }
        printf("\n");
    }

    // Sắp xếp hàng thứ hai theo thứ tự tăng dần
    for (i = 0; i < n; i++)
    {
        for (k = i + 1; k < n; k++)
        {
            if (a[1][i] > a[1][k])
            {
                int temp = a[1][i];
                a[1][i] = a[1][k];
                a[1][k] = temp;
            }
        }
    }
    printf("Mang sau khi sap xep hang thu hai theo thu tu tang dan:\n");
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("%1d ", a[i][j]);
        }
        printf("\n");
    }

    // Sắp xếp cột thứ hai theo thứ tự giảm dần
    for (i = 0; i < n; i++)
    {
        for (k = i + 1; k < n; k++)
        {
            if (a[i][1] < a[k][1])
            {
                int temp = a[k][1];
                a[i][1] = a[k][1];
                a[k][1] = temp;
            }
        }
    }
    printf("Mang sau khi sap xep cot thu hai theo thu tu giam dan:\n");
    for (i = 0; i < n; i++)
    {

        printf("%2d ", a[i][1]);
        printf("\n");
    }

    // Sắp xếp cột thứ hai theo thứ tự tăng dần
    for (i = 0; i < n; i++)
    {
        for (k = i + 1; k < n; k++)
        {
            if (a[i][1] > a[k][1])
            {
                int temp = a[k][1];
                a[i][1] = a[k][1];
                a[k][1] = temp;
            }
        }
    }
    printf("Mang sau khi sap xep cot thu hai theo thu tu tang dan:\n");
    for (i = 0; i < n; i++)
    {
        printf("%2d ", a[i][1]);
        printf("\n");
    }
}
