//Mã Văn Hùng - LTCB_L03_AT19E
#include <stdio.h>
#include <stdlib.h>

void avg_diagonal()
{
    int n;
    printf("Nhap n: ");
    scanf("%d", &n);
    int a[n][n];
    int b[n][n];
    int i, j;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("Nhap a[%d][%d]: ", i, j);
            scanf("%d", &a[i][j]);
        }
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("Nhap b[%d][%d]: ", i, j);
            scanf("%d", &b[i][j]);
        }
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("%d ", b[i][j]);
        }
        printf("\n");
    }

    int sum = 0;
    int count = 0;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            if (i == j)
            {
                sum += a[i][j];
                count++;
            }
        }
    }
    float avg = (float)sum / (float)count;
    printf("Tong trung binh duong cheo chinh cua ma tran a la: %f\n", avg);

    // Tính tổng trung bình đường chéo phụ của ma trận a
    int sum1 = 0;
    int count1 = 0;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            if (j = n - i - 1)
            {
                sum1 += a[i][j];
                count1++;
            }
        }
    }
    float avg1 = (float)sum1 / (float)count1;
    printf("Tong trung binh duong cheo phu cua ma tran a la: %f\n", avg1);

    // Tính tổng trung bình đường chéo chính của ma trận a
    int sum3 = 0;
    int count3 = 0;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            if (i == j)
            {
                sum += a[i][j];
                count3++;
            }
        }
    }
    float avg3 = (float)sum3 / (float)count3;
    printf("Tong trung binh duong cheo chinh cua ma tran b la: %f\n", avg);

    // Tính tổng trung bình đường chéo phụ của ma trận b
    int sum4 = 0;
    int count4 = 0;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            if (j = n - i - 1)
            {
                sum4 += b[i][j];
                count4++;
            }
        }
    }
    float avg4 = (float)sum4 / (float)count4;
    printf("sum trung binh duong cheo phu cua ma tran b la: %f\n", avg4);
}
