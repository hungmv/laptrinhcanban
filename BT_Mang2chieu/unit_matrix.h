//Mã Văn Hùng - LTCB_L03_AT19E
#include <stdio.h>

void unit_matrix()
{
    int n;
    printf("Nhap n: ");
    scanf("%d", &n);
    int a[n][n];
    int b[n][n];
    int i, j;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("Nhap a[%d][%d]: ", i, j);
            scanf("%d", &a[i][j]);
        }
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("Nhap b[%d][%d]: ", i, j);
            scanf("%d", &b[i][j]);
        }
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("%d ", b[i][j]);
        }
        printf("\n");
    }
    
    //Kiểm tra ma trận đơn vị
    int check = 0;
    for(i=0; i<n; i++)
    {
        for(j=0; j<n; j++)
        {
            if(i==j && a[i][i]==1 && a[i][j]==a[j][i])
            {
                check++;
            }else check=0;
        }
    }

    if(check>0)
    {
        printf("\nMa tran a don vi");
    }
    else printf("\nMa tran a khong phai ma tran don vi");

}
