//Mã Văn Hùng - LTCB_L03_AT19E
#include <stdio.h>

void min_max()
{
    int n;
    printf("Nhap n: ");
    scanf("%d", &n);
    int a[n][n];
    int b[n][n];
    int i, j;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("Nhap a[%d][%d]: ", i, j);
            scanf("%d", &a[i][j]);
        }
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("Nhap b[%d][%d]: ", i, j);
            scanf("%d", &b[i][j]);
        }
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("%d ", b[i][j]);
        }
        printf("\n");
    }
    //Tìm số lớn nhất trong ma trận
    //Tìm số nhỏ nhất trong ma trận
    int max = a[0][0];
    int min = a[0][0];
    int max1 = b[0][0];
    int min1 = b[0][0];
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            if (a[i][j] > max)
            {
                max = a[i][j];
            }
            if (a[i][j] < min)
            {
                min = a[i][j];
            }
        }
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            if (b[i][j] > max1)
            {
                max1 = b[i][j];
            }
            if (b[i][j] < min1)
            {
                min1 = b[i][j];
            }
        }
    }
    printf("So lon nhat trong ma tran a la: %d\n", max);
    printf("So nho nhat trong ma tran a la: %d\n", min);
    printf("So lon nhat trong ma tran b la: %d\n", max1);
    printf("So nho nhat trong ma tran b la: %d\n", min1);

}
