//Mã Văn Hùng - LTCB_L03_AT19E
#include <stdio.h>

int main()
{
    int n, i, j;
    printf("Nhap so luong phan tu: ");
    scanf("%d", &n);
    int a[n];
    for (i = 0; i < n; i++)
    {
        printf("a[%d] = ", i);
        scanf("%d", &a[i]);
    }
    // Sắp xếp mảng theo thứ tự tăng dần
    for (i = 0; i < n - 1; i++)
    {
        for (j = i + 1; j < n; j++)
        {
            if (a[i] > a[j])
            {
                int temp = a[i];
                a[i] = a[j];
                a[j] = temp;
            }
        }
    }
    printf("Mang sau khi sap xep: ");
    for (i = 0; i < n; i++)
    {
        printf("%d ", a[i]);
    }

    // In ngược lại mảng ban đầu
    printf("\nMang dao nguoc: ");
    for (i = n - 1; i >= 0; i--)
    {
        printf("%d ", a[i]);
    }

    // Tách mảng thành hai mảng chứa các phần tử chẵn và lẻ
    int even_arr[n], odd_arr[n], count = 0, count1 = 0;
    for (i = 0; i < n; i++)
    {
        if (a[i] % 2 == 0)
        {
            even_arr[count] = a[i];
            count++;
        }
        else
        {
            odd_arr[count1] = a[i];
            count1++;
        }
    }
    printf("\nMang chan: ");
    for (i = 0; i < count; i++)
    {
        printf("%d ", even_arr[i]);
    }
    printf("\nMang le: ");
    for (i = 0; i < count1; i++)
    {
        printf("%d ", odd_arr[i]);
    }
    return 0;
}