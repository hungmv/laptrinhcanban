//Mã Văn Hùng - LTCB_L03_AT19E
#include <stdio.h>

void tong(int *c, int *d)
{
    int sum = *c + *d;
    printf("Tong a + b = %d\n", sum);
}
void hieu(int *c, int *d)
{
    int hieu = *c - *d;
    printf("Hieu a - b = %d\n", hieu);
}
void tich(int *c, int *d)
{
    int tich = *c * *d;
    printf("Tich a * b = %d\n", tich);
}

float thuong(int *c, int *d)
{
    float thuong = *c / *d;
    return "a : b = %d ",thuong;
}
int main()
{
    int a, b, *c, *d;
    c = &a;
    d = &b;
    printf("Nhap a: ");
    scanf("%d", &a);
    printf("Nhap b: ");
    scanf("%d", &b);

    tong(c, d);
    hieu(c, d);
    tich(c, d);
    thuong(c, d);
}
