//Mã Văn Hùng - LTCB_L03_AT19E
#include <stdio.h>

int main ()
{
    int a[20][20], n, i, j, *p, min, max;
    printf("Nhap so luong phan tu: ");
    scanf("%d", &n);
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("a[%d][%d] = ", i, j);
            scanf("%d", &a[i][j]);
        }
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }
    int dem = 0, *b[20];
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            b[dem] = a[i][j];
            dem++;
        }
    }
    p = &b[i];
    min = *(p+0);
    max = *(p+0);
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            if (*(p + i) < min)
            {
                min = *(p + i);
            }
        }
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            if (*(p + i) > max)
            {
                max = *(p + i);
            }
        }
    }
    printf("\nMin = %d", min);
    printf("\nMax = %d", max);
}