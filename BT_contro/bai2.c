//Mã Văn Hùng - LTCB_L03_AT19E
#include <stdio.h>

int main()
{
    int a[20], n, i, j, *p, x, sum = 0;
    printf("Nhap so luong phan tu: ");
    scanf("%d", &n);
    for (i = 0; i < n; i++)
    {
        printf("a[%d] = ", i);
        scanf("%d", &a[i]);
    }
    p = &a[i];
    
    for (i = 0; i < n; i++)
    {
        sum += *(p + i);
    }
    printf("\nTong cac phan tu trong mang = %d", sum);

    for (i = 0; i < n - 1; i++)
    {
        for (j = i + 1; j < n; j++)
        {
            if (*(p + i) > *(p + j))
            {
                x = *(p + i);
                *(p + i) = *(p + j);
                *(p + j) = x;
            }
        }
    }
    printf("\nMang sau khi sap xep la: ");
    for (i = 0; i < n; i++)
    {
        printf("\n%d ", *(p + i));
    }
    
    return 0;
}

//dùng con trỏ sắp xếp mảng một chiều