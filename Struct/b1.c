// Mã Văn Hùng - LTCB_L03_AT19E
#include <stdio.h>

struct SinhVien
{
    char TenSV[20];
    float DiemTB;
};
typedef struct SinhVien SinhVien;
void nhapSV(SinhVien sv[], int n)
{
    for (int i = 0; i < n; i++)
    {
        printf("\nNhap thong tin sinh vien thu %d:", i);
        printf("\nNhap ten sinh vien:");
        fflush(stdin);
        gets(sv[i].TenSV);
        printf("\nNhap diem trung binh:");
        scanf("%f", &sv[i].DiemTB);
    }
}
void inSV(SinhVien sv[], int n)
{
    printf("\n-----THONG TIN SINH VIEN----\n");
    printf("TenSv \t\t\t DiemTB  \n");
    for (int i = 0; i < n; i++)
    {
        printf("%s \t\t\t %f \n", sv[i].TenSV, sv[i].DiemTB);
    }
}
void svGioi(SinhVien sv[], int n)
{
    printf("\n-----SINH VIEN DIEM GIOI----\n");
    printf("TenSv \t\t\t DiemTB>=8 \n");
    for (int i = 0; i < n; i++)
    {
        if (sv[i].DiemTB >= 8)
            printf("%s \t \t\t %f \n", sv[i].TenSV, sv[i].DiemTB);
    }
}
void svKha(SinhVien sv[], int n)
{
    printf("\n-----SINH VIEN DIEM KHA----\n");
    printf("TenSv \t\t\t DiemTB>=7 \n");
    for (int i = 0; i < n; i++)
    {
        if (sv[i].DiemTB >= 7)
            printf("%s \t \t\t %f \n", sv[i].TenSV, sv[i].DiemTB);
    }
}
void svYeu(SinhVien sv[], int n)
{
    printf("\n-----SINH VIEN DIEM YEU----\n");
    printf("TenSv \t\t\t DiemTB  \n");
    for (int i = 0; i < n; i++)
    {
        if (sv[i].DiemTB < 5)

            printf("%s \t\t\t %f \n", sv[i].TenSV, sv[i].DiemTB);
    }
}
void svCaonhat(SinhVien sv[], int n)
{
    SinhVien ss = sv[0];
    printf("\nSINH VIEN CO DIEM CAO NHAT\n");
    for (int i = 0; i < n; i++)
    {
        if (sv[i].DiemTB > ss.DiemTB)
        {
            ss = sv[i];
        }
    }
    printf("TenSv \t\t\t DiemTB  \n");
    printf("%s \t\t\t %f", ss.TenSV, ss.DiemTB);
}

int main()
{
    SinhVien sv[50];
    int n;
    printf("Nhap so sinh vien:");
    scanf("%d", &n);
    nhapSV(sv, n);
    inSV(sv, n);
    svGioi(sv, n);
    svYeu(sv, n);
    svCaonhat(sv, n);
}