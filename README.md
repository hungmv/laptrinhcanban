# Lập Trình Căn Bản - L03 - AT19
### Mã Văn Hùng AT130423

>**Bài tập mảng một chiều**

- [***Bài làm***](./BT_Mang1chieu/main.c)

<img src="./BT_Mang1chieu/btmang1chieu.jpg" alt="img BT" width="500"/>

**Bài tập mảng 2 chiều**

***Bài làm***
- [Tìm số lớn nhất, số nhỏ nhất trong ma trận](./BT_Mang2chieu/min_max.h)
- [Sắp xếp hàng 2, cột 2](./BT_Mang2chieu/sort.h)
- [Tính tổng trung bình đường chéo chính, phụ của ma trận](./BT_Mang2chieu/avg_diagonal.h)
- [Kiểm tra ma trận đơn vị](./BT_Mang2chieu/unit_matrix.h)
- [Tính tổng của hai ma trận](./BT_Mang2chieu/sum_matrix.h)
- [Tính tích của hai ma trận](./BT_Mang2chieu/multiplication.h)

***Đề bài***

<img src="./BT_Mang2chieu/btmang2chieu.jpg" alt="img BT" width="500"/>

**Bài tập về hàm**
- [***Bài làm***](./BT_Ham/Bai1.c)

***Đề bài***

<img src="./BT_Ham/debai_ham.jpg" alt="img BT" width="500"/>