// Mã Văn Hùng - LTCB_L03_AT19E
#include <stdio.h>

// ghi mảng một chiều vào file

void ghi_file(int a[100], int n, FILE *f)
{
    int i;
    for (i = 0; i < n; i++)
    {
        fprintf(f, "%d ", a[i]);
    }
}
void doc_file(FILE *f, int a[100], int n)
{
    int i, j;
    for (i = 0; i < n; i++)
    {
        fscanf(f, "%d", &a[i]);
    }
    printf("\n");
    for (i = 0; i < n; i++)
    {
        printf("%d ", a[i]);
    }
    for (i = 0; i < n - 1; i++)
    {
        for (j = i + 1; j < n; j++)
        {
            if (a[i] > a[j])
            {
                int temp = a[i];
                a[i] = a[j];
                a[j] = temp;
            }
        }
    }
    printf("\n");
    for (i = 0; i < n; i++)
    {
        printf("%d ", a[i]);
    }
}

int main()
{
    system("clear");
    int n, i;
    printf("Nhap so luong phan tu: ");
    scanf("%d", &n);
    int a[n];
    for (i = 0; i < n; i++)
    {
        printf("a[%d] = ", i);
        scanf("%d", &a[i]);
    }
    FILE *f;
    f = fopen("BAI1.txt", "w");
    ghi_file(a, n, f);
    fclose(f);
    f = fopen("BAI1.txt", "r");
    doc_file(f, a, n);
    fclose(f);
    f = fopen("BAI2.txt", "w");
    ghi_file(a, n, f);
    fclose(f);
    return 0;
}
